%define gtk4_version 4.12.5
%define libadwaita_version 1.4.2
%define upstream_version 2.1.0

Name:           raider
Version:        %upstream_version
Release:        alt1
Summary:        A GNOME desktop file shredding program designed for permanently removing files, with limitations on SSDs
Group:          File tools
BuildArch:      x86_64

License:        GPL-3.0
URL:            https://apps.gnome.org/ru/Raider/
Source0:        %{name}-%{version}.tar


BuildRequires(pre):  rpm-macros-meson
BuildRequires:  glib2-devel
BuildRequires:  libgtk4-devel >= %{gtk4_version}
BuildRequires:  libadwaita-devel >= %{libadwaita_version}
BuildRequires:  meson
BuildRequires:  desktop-file-utils
BuildRequires:  intltool
BuildRequires:  blueprint-compiler

Requires:       libgtk4 >= %{gtk4_version}
Requires:       libadwaita >= %{libadwaita_version}

%description
Raider is a dedicated file shredding application tailored for the GNOME desktop,
designed to permanently erase files from your computer. It offers effective file
deletion within certain limits. Notably, due to the inherent physical data writing
processes on SSDs, complete data eradication is challenging, making shredding not
entirely foolproof. However, it's important to note that generally only top-level
agencies possess the resources and technology to recover data under such
circumstances, requiring substantial time, effort, and financial investment. Raider
represents a robust solution for users seeking to enhance their data privacy and
security on the GNOME platform.

%prep
%setup -v


%build
%meson
%meson_build


%install
%meson_install
%find_lang %{name}
install -Dm644 LICENSE.md %{buildroot}%{_licensedir}/%{name}/LICENSE.md


%files -f %{name}.lang
%{_bindir}/%{name}
%{_datadir}/applications/com.github.ADBeveridge.Raider.desktop
%{_datadir}/icons/hicolor/256x256/apps/com.github.ADBeveridge.Raider.png
%{_datadir}/icons/hicolor/scalable/apps/com.github.ADBeveridge.Raider.svg
%{_datadir}/icons/hicolor/symbolic/apps/com.github.ADBeveridge.Raider-symbolic.svg
%{_datadir}/license/%{name}/LICENSE.md
%{_datadir}/metainfo/com.github.ADBeveridge.Raider.metainfo.xml


%changelog
* Wed Mar 27 2024 Aleksandr A. Voyt <vojtaa@basealt.ru> 2.1.0-alt1
- First package version
